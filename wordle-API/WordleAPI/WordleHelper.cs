﻿namespace WordleAPI
{
    public class WordleHelper
    {
        string[] wordList = File.ReadAllLines(@".\wordList.txt");

        public String GetWordleReturn(bool isTest)
        {
            var wordle = isTest ? "wordl" : GetRandomWord();
            return wordle;
        }
        public String GetRandomWord()
        {
            var random = new Random();
            var word = random.Next(1, wordList.Length);
            return GetWord(word);
        }

        public String GetWord(int index)
        {
            return wordList[index];
        }
    }
}
