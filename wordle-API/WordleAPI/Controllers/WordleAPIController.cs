﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace WordleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Wordle : ControllerBase
    {
        WordleHelper wordleHelper = new WordleHelper();
        [EnableCors("_myAllowSpecificOrigins")]
        [HttpGet(Name = "Wordle")]
        public string Get(bool isTest)
        {
            return wordleHelper.GetWordleReturn(isTest);
        }
    }
}