using NUnit.Framework;
using System;
using System.IO;
using WordleAPI;

namespace WordleTests
{
    public class Tests
    {
        private WordleHelper wordleHelper = new WordleHelper();
        private string[] wordList = File.ReadAllLines(@".\WordList.txt");

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test_TestWordReturned()
        {
            var isTestTrue = wordleHelper.GetWordleReturn(true);
            Assert.IsTrue(isTestTrue.CompareTo("wordl") == 0, "The returned word should equal: 'wordl' , But was: " + isTestTrue);
            var isTestFalse = wordleHelper.GetWordleReturn(false);
            Assert.IsFalse(isTestFalse.CompareTo("wordl") == 0, "The returned word should not equal: 'wordl' , But was: " + isTestFalse);
        }

        [Test]
        public void Test_GetRandomWordReturnsString()
        {
            Assert.IsInstanceOf(typeof(String), wordleHelper.GetRandomWord());
        }

        [Test]
        public void Test_GetRandeomWordReturnsRandomWords()
        {
            var word1 = wordleHelper.GetRandomWord();
            var word2 = wordleHelper.GetRandomWord();
            if (word1 == word2)
            {
                word2 = wordleHelper.GetRandomWord();
            }
            Assert.AreNotEqual(word1, word2);
        }

        [Test]
        public void Test_ValidateGetWordReturn()
        {
            for (int index = 0; index < wordList.Length; index++)
            {
                Assert.IsTrue(wordleHelper.GetWord(index).CompareTo(wordList[index]) == 0, "The returned word should equal: " + wordList[index] + " , But was: " + wordleHelper.GetWord(index));
            }
        }
    }
}